
import {useState, useEffect} from 'react'
import Data from './../components/Banner'
import CourseCard from './../components/CourseCard'
import {Container} from 'react-bootstrap'
const data = {
	title: 'Course Catalog',
	content: 'Browse through our Catalog of courses'

}
export default function Courses (){
	const [coursesCollection,setCourseCollection] = useState([]);

	useEffect(()=>{

		fetch('https://agile-brushlands-62754.herokuapp.com/courses/active').then(res => res.json()).then(convertedData =>{
			// console.log(convertedData)
			setCourseCollection(convertedData.map(course =>{
				return(

					<CourseCard key={course._id} courseProp={course}/>
					)
			}))
		})
	},[]);
	return(
		<>

		<Data bannerData={data}/>
		<Container>
			{coursesCollection}
		</Container>
		
		</>
	)
}