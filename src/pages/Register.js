import Course from './../components/Banner'
import {Container,Form,Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import {Navigate} from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'

const details ={
	title: "Welcome to the Register",
	content: "Create an account to enroll"
}	
export default function Register() {
	const {user} = useContext(UserContext)

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [isMatched, setIsMatched] = useState(false);
	const [isMobileValid,setIsMobileValid] = useState(false);
	const [isAllowed,setIsAllowed] = useState(false)

	useEffect(()=>{
					
		if (mobileNo.length === 11) {
			setIsMobileValid(true)
		
			if (password1 === password2 && password1 !== '' && password2 !== '') {
			setIsMatched(true)
			}
			if (firstName !== '' && lastName!== '' && email !== ''){
				setIsAllowed(true)
				setIsActive(true)
			}
		}
		else if(password1 !== '' && password1 === password2) {
			setIsMatched(true);
		}
		else {

			setIsMobileValid(false)
			setIsMatched(false)
			setIsActive(false)
			setIsAllowed(false)
		}
	},[firstName,lastName,email,password1,password2,mobileNo])


	const registerUser = async (eventSubmit)=>{
		eventSubmit.preventDefault()

		const isRegistered = await fetch(`https://agile-brushlands-62754.herokuapp.com/users/register`,{
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
			"firstName": firstName,
			"lastName":lastName,
			"email":email,
			"password":password1,
			"mobileNo":mobileNo
			})

		}).then(res => res.json()).then(dataJSONderulo =>{
			
			if (dataJSONderulo.email) {
				return true
			} else {
				return false
			}
		})
		if (isRegistered) {
				
				await Swal.fire({
		 		icon: 'success',
				title: 'Registration Successful',
		 		text:'Thank you for signing in'
				});

				setFirstName('')	 
				setLastName('')
				setEmail('')
				setMobileNo('')
				setPassword1('')
				setPassword2('')

		
				window.location.href = "/login";

			} else {
				Swal.fire({
					icon: 'error',
					title: 'Something Went Wrong',
					text: 'Please try again'
				})
			}
		 		
	};

	return(
		user.id ? 
		<Navigate to ='/courses' replace={true}/>
		:
		<div>
		
		<Course bannerData={details} />

		<Container>
			{
				isAllowed ? 
			<h1 className = "text-center text-success">You may now register!</h1>

				:
			<h1 className = "text-center">Register Form</h1>
			}
			<h6 className="text-center mt-3 text-secondary">Fill Up the form balow</h6>
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter your first name" required value={firstName} onChange ={event => setFirstName(event.target.value)}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter your last name" required value={lastName}onChange ={event => setLastName(event.target.value)}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" placeholder="Insert your email address" required value={email}onChange ={event => setEmail(event.target.value)}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile No.:</Form.Label>
					<Form.Control type="number" placeholder="Insert your mobile number" required value={mobileNo}onChange ={event => setMobileNo(event.target.value)}/>
					{
						isMobileValid ?
							<span className= "text-success">Mobile No. valid</span>
						: 
							<span className= "text-muted">Mobile No. Should be 11 digits</span>
					}
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Insert your password" required value={password1}onChange ={event => setPassword1(event.target.value)}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password:</Form.Label>
					<Form.Control type="password" placeholder="Confirm your password" required value={password2}onChange ={event => setPassword2(event.target.value)}/>
					{isMatched ?
					<span className="text-success">Passwords Matched!</span>
					:
					<span className="text-danger">Passwords should match!</span>
				}
				</Form.Group>
				
				
				{isActive ?
					<Button className=" btn-success btn-block" type="submit">Register</Button>
					:
					<Button className=" btn-secondary btn-block" disabled>Register</Button>
				}
				
				
			</Form>
		</Container>
		
		</div>
		);
	}
