import Update from './../components/Banner'
import {Form,Button,Container} from 'react-bootstrap'
import Swal from 'sweetalert2'
const data = {
	title: 'Update your courses here',
	content: 'Update your courses below'
}

export default function UpdateCourse(){
	const updatecourse = (eventSubmit) =>{
		eventSubmit.preventDefault()
		return(
			
			Swal.fire({
				icon: 'Success',
				title: 'You have updated a course',
				text: "Success!"
			})	
			
			);
	};
	return(
	<>
		<Update bannerData = {data}/>
		<Container>
		<h1 className="text-center">Course</h1>
		<Form onSubmit={e => updatecourse(e)}>
			<Form.Group>
				<Form.Label>Course Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter Course Name Here" required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Course Description:</Form.Label>
				<Form.Control type="text" placeholder="Enter Description Here" required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Price:</Form.Label>
				<Form.Control type="text" placeholder="Enter Price" required/>
			</Form.Group>
			 <Form.Check 
				type="switch"
				label="Enable Course"
				id="enabled-custom-switch"
				  />
			<Button className='btn-block' variant= 'success' type='submit'>Submit</Button>
			
		</Form>
		</Container>
	</>
	)
}