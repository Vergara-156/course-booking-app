import {Row, Col, Card, Button, Container} from 'react-bootstrap'
import { useState, useEffect} from 'react'
import {Link, useParams} from 'react-router-dom'
import CourseView from './../components/Banner'
import Swal from 'sweetalert2'
const data = {
	title: 'Welcome to B156 Booking-App',
	content: 'Check out our school campus'
}

export default function Course(){
	const [courseInfo,setCourseInfo] = useState({
		name: null,
		description: null,
		price: null
	})
	console.log(useParams())
	const {id} = useParams()
	console.log(id)
	useEffect (() => {

		fetch(`https://agile-brushlands-62754.herokuapp.com/courses/${id}`).then(res => res.json()).then(convertedData =>{ 
			console.log(convertedData);
			setCourseInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			})
		})

	},[id])
	const enroll = () => {
		return(
			Swal.fire(
			{	icon: 'success',
				title: 'Enrolled Successfully',
				text:'Thank you for enrolling to this course'
			})
		);
	};
	return(
	<>
	<CourseView bannerData = {data}/>
	<Row>
		<Col>
			<Container>
			<Card className="text-center">
				<Card.Body>
					<Card.Title>
						<h2>{courseInfo.name}</h2>
					</Card.Title>
					<Card.Subtitle>
						<h6 className="my-4">Description</h6>
						</Card.Subtitle>
					<Card.Text>{courseInfo.description}
					</Card.Text>
					<Card.Subtitle>
						<h6 className="my-4">PHP: {courseInfo.price}</h6>
						</Card.Subtitle>
				</Card.Body>
				
				<Button variant= "warning" className="btn-block" onClick={enroll}>
					Enroll
				</Button>
				<Link className="btn btn-success btn-block mb-5" to ="/login">Login to Enroll
					
				</Link>

				
			</Card>
			</Container>
		</Col>
	</Row>
	</>
	)
}

{/* Comments*/}