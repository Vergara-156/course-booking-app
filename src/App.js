import {useState, useEffect} from 'react';
import AppNavBar from './components/AppNavBar'
import Home from './pages/Home'
import Register from './pages/Register'
import Courses from './pages/Courses'
import Error from './pages/Error'
import Login from './pages/Login'
import CourseView from './pages/CourseView'
import AddCourse from './pages/AddCourse'
import Logout from './pages/Logout'
import UpdateCourse from './pages/UpdateCourse'
import {UserProvider} from './UserContext'

import  {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import  './App.css';
function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })
const unsetUser = () => {
  localStorage.clear()
  setUser({
    id:null,
    isAdmin: null
  })
}
useEffect(()=>{
  let token = localStorage.getItem('accessToken');
    // console.log(token)
    fetch('https://agile-brushlands-62754.herokuapp.com/users/details/',{
      headers:{
        Authorization: `Bearer ${token}`
      }
    }).then(res =>res.json()).then(convertedData =>{ 
      // console.log(convertedData)

      if (typeof convertedData._id !== "undefined") {
          setUser({
            id: convertedData._id,
            isAdmin: convertedData.isAdmin
          });
         // console.log(user)
    } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
    
},[user])

  return (
    <UserProvider value ={{user, setUser ,unsetUser}}>
      <Router>
        <AppNavBar/>
        <Routes>
         <Route path= '/register' element={<Register/>} />
         <Route path= '/' element={<Home/>} />
         <Route path= '/courses' element={<Courses/>}/>
         <Route path= '/course' element={<AddCourse/>}/> 
         <Route path= '/login' element={<Login/>}/>
         <Route path= 'course/view/:id' element={<CourseView/>}/>
         <Route path= '/logout' element= {<Logout/>}/>
         <Route path= '/update' element= {<UpdateCourse/>}/>
         <Route path= '*' element={<Error/>}/>
        </Routes>
       </Router>
     
     </UserProvider>
  );
}

export default App;
