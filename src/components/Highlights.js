import {Row, Col, Card, Container} from 'react-bootstrap'

export default function Highlights(){
	return( 
		<Container>
			<Row className="my-3">
				<Col xs={12} md={4}>
					<Card className = "p-4 cardHighlight">
						<Card.Title> Learn From Home</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga, quo.
						</Card.Text>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className = "p-4 cardHighlight">
						<Card.Title> Study Now Pay later</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga, quo.
						</Card.Text>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className = "p-4 cardHighlight">
						<Card.Title>Be Part of our Community</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga, quo.
						</Card.Text>
					</Card>
				</Col>


			</Row>
		</Container>
		
		)
}