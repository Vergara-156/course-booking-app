import {useContext} from 'react'
import {Navbar, Nav, Container} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'
function AppNavBar(){
	const {user} = useContext(UserContext)
	return(
		<Navbar bg="primary" expand= "lg">
		<Container>
		<Navbar.Brand>B156 Booking App</Navbar.Brand>
		<Navbar.Toggle aria-controls = "basic-navbar-nav" />
		<Navbar.Collapse>
			<Nav className="ml-auto">
				<Link to ="/" className="nav-link">Home
					
				</Link>
				{user.id !== null ?
				<Link to ="/logout" className="nav-link">Log-out</Link>
					:
				<>
				<Link to ="/login" className="nav-link">Log-in</Link>
				<Link to ="/register" className="nav-link">Register</Link>
				</>
				

				}
				
				<Link to ="/courses" className="nav-link">Courses
					
				</Link>
				
				
				<Link to ="/course/view" className="nav-link">Course
					
				</Link>
				<Link to ="/course" className="nav-link">Add Course
					
				</Link>
				<Link to ="/update" className="nav-link">Update Course
					
				</Link>
			</Nav>
		</Navbar.Collapse>
		</Container>
		</Navbar>
		);
};

export default AppNavBar;