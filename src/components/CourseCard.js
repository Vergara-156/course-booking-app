import {Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'
export default function Highlights({courseProp}){
	return( 

		<Card className ="courseCard m-4 d-md-inline-flex d-sm-inline-flex" >
			<Card.Body>
				<Card.Title>{courseProp.name}</Card.Title>
				<Card.Text>{courseProp.description}</Card.Text>
				<Card.Text>P{courseProp.price}</Card.Text>
				<Link to={`/course/view/${courseProp._id}`} className="btn btn-primary">View Course</Link>
			</Card.Body>
		</Card>

		)}